## VueLaravel - CRUD Aplicacion.

Esta es una aplicación cliente/servidor que maneja peticiones GET, POST. En donde se puede guardar data de usuarios directamente en BD.

La misma permite modificar data existente, y eliminar adicionalmente.

- [Laravel](https://laravel.com//)
- [VueJS](https://vuejs.org/)
- [Axios](https://github.com/axios/axios)
- [Bootstrap](https://getbootstrap.com/)

Para ejecutarlo en modo de desarrollo:

### `composer create-project --prefer-dist laravel/laravel users`

### `php artisan serve`

### `npm install vue vue-router vue-axios`